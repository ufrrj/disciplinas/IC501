#!/usr/local/bin/python3.9

'''
    Funcoes matematicas

    log
    sin
    cos
    tan
    atan
    exp

'''
import math as m

angulo = float(input("Digite o Angulo em graus: "))

rad = m.pi * angulo / 180
lista = range(1,10)

print( "    Seno", angulo, " = ", m.sin(rad) )
print( "  Coseno", angulo, " = ", m.cos(rad) )
print( "Tangente", angulo, " = ", m.tan(rad) )
print( "    Seno hip", angulo, " = ", m.sinh(rad) )
print( "  Coseno hip", angulo, " = ", m.cosh(rad) )
print( "Tangente hip", angulo, " = ", m.tanh(rad) )
print( "Valor e = ", m.exp(1) )
print( "Raiz quadrada", angulo, m.sqrt(angulo) )
print( "Elevado ao quadrado = ", angulo ** 2, angulo * angulo )
print( "Raiz Cubica = ", angulo ** (1/3) )
print( "Raiz quinta ordem = ", angulo ** (1/5) )
print( "logaritimo", angulo, " = ", m.log(angulo) )
print( "logaritimo", angulo, " = ", m.log2(angulo) )
print( "Soma",lista, m.fsum(lista)/len(lista) )
soma = 0
for elem in lista :
    soma = soma + elem
print( "Soma", soma )
