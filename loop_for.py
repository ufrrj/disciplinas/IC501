#!/usr/local/bin/python3.9
'''
    Programa exemplo para loop com o comando WHILE
    
    Sintaxe:

    for VAR in LISTA :
        COMANDOS

    Note que os COMANDOS dentro do loop DEVEM estar aninhados com espaçamento
    maior, para que o nível de hierarquia seja obedecido

'''

frase = input("Digite ume frase: ")
for letra in frase :
    print(letra)

print("---")

tam = 0
for letra in frase :
    tam = tam + 1
print(tam)
print("---")

'''
FAÇA V =Vi ATE Vf [PASSO P]

    função range
    Sintaxe:

    range( Vi, Vf, P )

    Vi = Valor Inicial
    Vf = Valor Final + 1 
    P  = Passo, É opcional, valor padrào é 1

'''
for i in range(1,11) :
    print(i)

print("---")
for i in range(10, 0, -1) :
    print(i)

print("---")
for i in range(10, 0, -3) :
    print(i)

print("---")

