#!/usr/local/bin/python3.9
'''
    Programa exemplo para loop com o comando WHILE
    
    Sintaxe:

    while ( EXP ) :
        COMANDOS

    Note que os COMANDOS dentro do loop DEVEM estar aninhados com espaçamento
    maior, para que o nível de hierarquia seja obedecido

'''

frase = input("Digite ume frase: ")
index = 0
tam = len(frase)
while ( index < tam ) :
    print(frase[index])
    index = index + 1

'''
    Bloco com erro de alinhamento/aninhamento
index = 0
while ( index < tam ) :
print ( frase [ index ] )
index = index + 1
'''

