#!/usr/local/bin/python3.9
'''
    Comentário de bloco
    pode conter várias linhas sem necessidade de quebrar

'''
cep = input("Digite o CEP: ")
valido = True
tam = len(cep)
c = 0
if ( cep[2] == "." and cep[6] == "-" and tam == 10):
    while ( c < tam and valido ):
        if ( c != 2 and c != 6 ):
            if ( cep[c] >= "0" and cep[c] <= "9" ) : 
                valido = True
            else :
                valido = False
        c = c + 1
else :
    valido = False
if ( valido ) :
    print("CEP ", cep, "e valido")
else :
    print("CEP ", cep, "NAO e valido")
